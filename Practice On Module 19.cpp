#include <iostream>

using namespace std;

class Animal
{

public:

    virtual void Voice()
    {
        cout << " There is some animals !";
    }
};

class Dog : public Animal
{

public:

    void Voice() override
    {
        cout << " Woof !";
    }
};

class Cat : public Animal
{
public:

    void Voice() override
    {
        cout << " Meow !";
    }
};

class Rat : public Animal
{
public:

    void Voice() override
    {
        cout << " Pee !\n";;
    }
};


int main()
{

    Animal* p = new Dog;
    Animal* p1 = new Cat;
    Animal* p2 = new Rat;

    const int N = 3;
    Animal* Array[N] = { p,p1,p2 };

    for (int i = 0; i < N; i++)
    {
        Array[i]->Voice();
    }


    return 0;
}
